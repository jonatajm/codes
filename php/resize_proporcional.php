public function resizeProporcional($filePath)
    {
        //check if path exists
        if (is_dir($filePath)) {
            //get all files of the path
            $files = File::allFiles($path."tmp");
            //define the size limits of the image
            $resizeConfig = [
                "width" => 1452,
                "height" => 818
            ];
            foreach ($files as $file) {
                //get the sizes of the image
                list($widthImage, $heightImage) = getimagesize($path."tmp/".$file->getBasename());
                $ratio = $resizeConfig['width'] <= 0 ? 1 : ($widthImage / $resizeConfig['width']);
                $varHeight = $heightImage / $ratio;
                //after define de new sizes,verify if height obey the limits
                if ($varHeight > $resizeConfig['height']) {
                    //if not define a new ratio
                    $ratio = $resizeConfig['height'] <= 0 ? 1 : ($heightImage / $resizeConfig['height']);
                    $varHeight = $heightImage / $ratio;
                }
                $varWidth = $widthImage / $ratio;
                
                //make a new Image with the new sizes
                Image::make($file)->resize($varWidth, $varHeight)->save($path.$file->getBasename());
            }
        }
    }