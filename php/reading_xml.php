//xml
$xmlStr="<?xml version='1.0'?>
<cars>
   <car id='001'>
      <owner>harry</name>
      <email>harry@harry.com</email>
   </car>
   <car id='002'>
      <owner>jhon</name>
      <email>jhon@jhon.com</email>
   </car>
</cars>";

//read xml
$xml =simplexml_load_string($xmlStr);

//search for each occurrence of 'car'
foreach ($xml->car as $car)
{
   echo $car['id'].'  '.$car->owner.'  '.$car->email.'<br />';
}