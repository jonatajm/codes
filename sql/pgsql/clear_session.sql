--check actives sessions
select * from pg_stat_activity;

--clear sessions
SELECT pg_terminate_backend(pid)
  FROM pg_stat_activity
  WHERE pid <> pg_backend_pid();

