DO $$
DECLARE
i TEXT;
BEGIN
  FOR i IN (SELECT tbls.table_name FROM information_schema.tables AS tbls INNER JOIN information_schema.columns AS cols ON tbls.table_name = cols.table_name WHERE tbls.table_schema = 'public' and tbls.table_type = 'BASE TABLE' AND cols.column_name='id') LOOP
      EXECUTE 'SELECT setval(''"' || i || '_id_seq"'', (SELECT (MAX(id) + 1) as id FROM ' || quote_ident(i) || '));';
  END LOOP;
END $$