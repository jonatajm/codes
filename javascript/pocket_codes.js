//when click on a element with class 'remove-file-custom'
$('.remove-file-custom').click(function() {
    //look for a 'input' element and get id, in the container div of the buttom clicked
    var inputId = $(this).parent().find('input')[0].id;
    //remove elements of the container div
    $(this).parent().find('img').remove();
    $(this).parent().find('a').remove();
    
    //set value with the 'id' gotten previously
    if ($('#imagesToDelete').val().substr(0, $('#imagesToDelete').val().lenght) == "") {
        $('#imagesToDelete').val(inputId);
    } else {
        $('#imagesToDelete').val($('#imagesToDelete').val()+','+inputId);
    }
});

//for each element with the class 'colorpicker' set the type of the field to a color picker and show the color setting the background-color of another div
$('.colorpicker').each(function () {
    $(this).colorpicker();
    $(this).parent().find('div>div').css('background-color', $(this).parent().find('input').val())
});