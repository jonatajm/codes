<?php

namespace App\Http\Controllers\Main;

use App\Http\Requests\StoreExamplesRequest;
use App\Http\Requests\UpdateExamplesRequest;
use App\Example;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
class ExamplesController extends Controller
{

    /**
     * Display a listing of Example.
     *
     * @return \Illuminate\Http\Response
     */
    private function getTemplate($template, $params) {
        return view($template, $params);
    }
    public function index()
    {
        $query = Example::query();
        $datas = $query->select([
            'examples.id',
            'examples.descricao',
        ])->get();
        $tableBody = "";
        if (count($datas) > 0) {
            $gateKey  = 'example_';
            $routeKey = 'pages.exaples';
            foreach ($datas as $data) {
                $tableBody .= "<tr>
                                <td style='width: 40px;'><input type='checkbox' class='selectable' value='{$data->id}' name='checkBox[]' /></td>
                                <td>{$data->descricao}</td>
                                <td style='width: 155px;'>
                                    ".$this->getTemplate('layouts.actionsTemplate', ['row'=>$data, 'gateKey'=>$gateKey, 'routeKey'=>$routeKey])."
                                </td>
                            </tr>";
            }
        } else {
            $tableBody .= "<tr>
                                <td colspan='3' style='text-align: center;'>Nenhum resultado encontrado</td>
                            </tr>";
        }
        $table = "<table id=\"tabelaExamples\" class=\"table table-bordered table-hover tabelaDados\">
                        <thead>
                            <tr>
                                <th><input type='checkbox' class='selecionarTodos' /></th>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            {$tableBody}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><input type='checkbox' class='selecionarTodos' /></th>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </tfoot>
                    </table>";
        return view('pages.examples.index', compact('table'));
    }

    /**
     * Show the form for creating new Example.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.examples.create');
    }

    /**
     * Store a newly created Example in storage.
     *
     * @param  \App\Http\Requests\StoreProjectsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExamplesRequest $request)
    {
        $dataRequest = $request->all();
        Example::create($dataRequest);

        return redirect()->route('pages.examples.index');
    }


    /**
     * Show the form for editing Example.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Example::findOrFail($id);

        return view('pages.examples.edit', compact('data'));
    }

    /**
     * Update Example in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExamplesRequest $request, $id)
    {
        $update = Example::findOrFail($id);
        $update->update($request->all());

        return redirect()->route('pages.examples.index');
    }


    /**
     * Display Example.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Example::findOrFail($id);

        return view('pages.examples.show', compact('data'));
    }


    /**
     * Remove Example from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Example::findOrFail($id);
        $delete->delete();

        return redirect()->route('pages.examples.index');
    }

    /**
     * Delete all selected Example at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Example::whereIn('id', $request->input('ids'))->get();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
