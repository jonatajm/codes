@extends('layouts.default')
@section('content')
            <div class="card">
                <div class="card-header">
                    <a href="{{route('pages.examples.mass_destroy')}}" style="float: right;" class="btn btn-xs btn-danger massDestroy">
                        <i class="fa fa-trash"></i>
                    </a>
                    <a href="{{route('pages.examples.create')}}" style="float: right;" class="btn btn-xs btn-success">
                        <i class="fa fa-plus-square"></i>
                    </a>
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Examples</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!!$table!!}
                </div>
                <!-- /.card-body -->
            </div>
@stop