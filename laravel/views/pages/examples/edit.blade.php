@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header row">
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Edit example</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['pages.examples.update', $data->id]]) !!}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    {!! Form::label('field2', 'Nome', ['class' => 'control-label']) !!}
                                    <input type="text" name="field2" value="{{$data->field2}}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
                    <a href="{{ route('pages.examples.index') }}" class="btn btn-default">Back</a>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop